function limitFunctionCallCount(cb,n){
return function(){
    if(n>0){
        for(let index=0;index<n;index++){
           cb();
        }
        return "cb called...";
    }
    else{
        return null;
    }
}
}
module.exports=limitFunctionCallCount;