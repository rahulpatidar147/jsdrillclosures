function counterFactory(){
let counter=10;

return function() {
    function increment(){
        counter+=1;
        return counter;
    }
    function decrement(){
        counter-=1;
        return counter;
    }
    return {increment,decrement};
}
}
module.exports=counterFactory;