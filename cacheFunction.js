function cacheFunction(cb) {
let cache ={};    
return function(...args){
  let uniqueArgu ="";
    if(cache.hasOwnProperty(uniqueArgu)){
      return cache[uniqueArgu];
    }
    else{
      cache[uniqueArgu]=cb(...args);
      return cache[uniqueArgu];

    }
};
}
module.exports=cacheFunction;
